using Foundation;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using UIKit;

namespace logInLogOut
{
    public partial class ChangePwdViewController : UIViewController
    {
        public ChangePwdViewController(IntPtr handle) : base(handle)
        {
        }

        partial void HideModelBtn_TouchUpInside(UIButton sender)
        {
            DismissModalViewController(true);
        }

        partial void CancelBtn_TouchUpInside(UIButton sender)
        {
            DismissModalViewController(true);
        }

        public class ModifyPwd
        {
            public string currentPassword { get; set; }
            public string newPassword { get; set; }
            public ModifyPwd(string oldP, string newP)
            {
                currentPassword = oldP;
                newPassword = newP;
            }
        }

        async partial void ConfirmBtn_TouchUpInside(UIButton sender)
        {
            if (confirmPwdTextField.Text == newPasswordTextField.Text)
            {
                ModifyPwd modifyPwd = new ModifyPwd(oldPasswordTextField.Text, newPasswordTextField.Text);
                string json = JsonConvert.SerializeObject(modifyPwd);

                var PostSuccess = await PostPassword(json);
                ShowMsg(PostSuccess);
            }else{
                confirmPwdTextField.Layer.BorderColor = UIColor.Red.CGColor;
                confirmPwdTextField.Layer.BorderWidth = 1f;

                newPasswordTextField.Layer.BorderColor = UIColor.Red.CGColor;
                newPasswordTextField.Layer.BorderWidth = 1f;
            }
        }

        private void ShowMsg(bool success)
        {
            string title;
            string message;
            if(success){
                title = "Success";
                message = "You have changed your password!";
            }else{
                title = "Sorry";
                message = "Fail to change your password ><";
            }
            //create alert
            var alert = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);

            //create an option "OK" that changes view when clicked
            alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, (UIAlertAction obj) => {
                DismissModalViewController(true);
            }));

            //display the alert as a popover view on top of the current view
            PresentViewController(alert, true, null);
        }

        async public Task<bool> PostPassword(string json)
        {
            try
            {
                var plist = NSUserDefaults.StandardUserDefaults;
                var t = plist.StringForKey("userToken");

                var client = new HttpClient();
                string url = "http://sample.dentall.io/api/account/change-password/";
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", t);
                var response = await client.PostAsync(url, content);

                Debug.WriteLine(response);
                int status = (int)response.StatusCode;

                return (status == 200);
            }catch(Exception ex){
                var alert = UIAlertController.Create("Oops!", ex.Message.ToString(), UIAlertControllerStyle.Alert);

                //another way: do nothing when the option is clicked
                alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

                //display the alert as a popover view on top of the current view
                PresentViewController(alert, true, null);
                return (false);
            }
        }

    }

}
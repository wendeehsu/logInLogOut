﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using UIKit;
using Foundation;

namespace logInLogOut
{
    public partial class ViewController : UIViewController
    {
        protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }


        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            UiIndicator.Hidden = true;  

        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        public class User{
            public string password { get; set; }
            public string username { get; set; }
            public bool rememberMe { get; set; }
        }

        partial void SwitchValueChange(UISwitch sender)
        {
            bool rememberMe = rememberMeSwitch.On;
            if(rememberMe){
                rememberMeText.TextColor = UIColor.Green;
            }else{
                rememberMeText.TextColor = UIColor.Gray;
            }

            var plist = NSUserDefaults.StandardUserDefaults;
            plist.SetBool(rememberMe, "RememberMe");
        }

        public void ReType()
        {
            Warning.Text = "Invalid username or password";
            Account.Text = "";
            Password.Text = "";

            Account.Layer.BorderColor = UIColor.Red.CGColor;
            Account.Layer.BorderWidth = 1f;

            Password.Layer.BorderColor = UIColor.Red.CGColor;
            Password.Layer.BorderWidth = 1f;
        }

        async partial void LogInBtn_TouchUpInside(UIButton sender)
        {
            User newUser = new User();
            newUser.password = Password.Text.ToString();
            newUser.username = Account.Text.ToString();
            var plist = NSUserDefaults.StandardUserDefaults;
            newUser.rememberMe = plist.BoolForKey("RememberMe");

            // The error in the next line will work fine while running
            string json = JsonConvert.SerializeObject(newUser);

            //show UiIndicator
            UiIndicator.Hidden = false;
            UiIndicator.StartAnimating();
            var logInSuccess = await PostFuncAsync(json);
            Debug.WriteLine(logInSuccess);
            UiIndicator.StopAnimating();
            UiIndicator.Hidden = true;

            if(!logInSuccess){
                ReType();
            }else{
                JumpToLogInPage();
            }
        }

        public void JumpToLogInPage()
        {
            Warning.Text = "";

            Account.Layer.BorderColor = UIColor.Gray.CGColor;
            Account.Layer.BorderWidth = 0f;

            Password.Layer.BorderColor = UIColor.Gray.CGColor;
            Password.Layer.BorderWidth = 0f;

            var storyboard = UIStoryboard.FromName("Main", null);
            var mainController = storyboard.InstantiateViewController("LogIn") as LogInPageViewController;
            this.NavigationController.PushViewController(mainController, true);

        }

        async public Task<bool> PostFuncAsync(string json)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri("http://sample.dentall.io");

                var content = new StringContent(json, Encoding.UTF8, "application/json");
                HttpResponseMessage response = null;
                response = await client.PostAsync("/api/authenticate", content);

                // this result string should be something like: "{"token":"rgh2ghgdsfds"}"
                var result = await response.Content.ReadAsStringAsync();

                int status = (int)response.StatusCode;
                if (status == 200)
                {
                    var definition = new { id_token = "" };
                    var id1 = JsonConvert.DeserializeAnonymousType(result, definition);

                    string token = Convert.ToString(id1.id_token);
                    //Debug.WriteLine(token);

                    var t = NSUserDefaults.StandardUserDefaults;
                    t.SetString(token, "userToken");
                    Debug.WriteLine(token);
                }
                return (status == 200);
            }
            catch(Exception ex){
                var alert = UIAlertController.Create("Oops!", ex.Message.ToString(), UIAlertControllerStyle.Alert);

                //another way: do nothing when the option is clicked
                alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

                //display the alert as a popover view on top of the current view
                PresentViewController(alert, true, null);
                return (false);
            }

        }



        partial void CreateAccount_TouchUpInside(UIButton sender)
        {
            //jump to Create Account Page
        }


        partial void ForgetPwdBtn_TouchUpInside(UIButton sender)
        {
            //create alert
            var alert = UIAlertController.Create("Oops", "我很遺憾", UIAlertControllerStyle.Alert);


            //another way: do nothing when the option is clicked
            alert.AddAction(UIAlertAction.Create("ＱＱ", UIAlertActionStyle.Default, null));

            //display the alert as a popover view on top of the current view
            PresentViewController(alert, true, null);
        }
    }
}

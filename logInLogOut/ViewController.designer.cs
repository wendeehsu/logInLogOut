// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace logInLogOut
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField Account { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton CreateAccount { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton ForgetPwdBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UINavigationItem HelloPage { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton LogInBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField Password { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISwitch rememberMeSwitch { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel rememberMeText { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Title { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIActivityIndicatorView UiIndicator { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Warning { get; set; }

        [Action ("CreateAccount_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void CreateAccount_TouchUpInside (UIKit.UIButton sender);

        [Action ("ForgetPwdBtn_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void ForgetPwdBtn_TouchUpInside (UIKit.UIButton sender);

        [Action ("LogInBtn_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void LogInBtn_TouchUpInside (UIKit.UIButton sender);

        [Action ("SwitchValueChange:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void SwitchValueChange (UIKit.UISwitch sender);

        void ReleaseDesignerOutlets ()
        {
            if (Account != null) {
                Account.Dispose ();
                Account = null;
            }

            if (CreateAccount != null) {
                CreateAccount.Dispose ();
                CreateAccount = null;
            }

            if (ForgetPwdBtn != null) {
                ForgetPwdBtn.Dispose ();
                ForgetPwdBtn = null;
            }

            if (HelloPage != null) {
                HelloPage.Dispose ();
                HelloPage = null;
            }

            if (LogInBtn != null) {
                LogInBtn.Dispose ();
                LogInBtn = null;
            }

            if (Password != null) {
                Password.Dispose ();
                Password = null;
            }

            if (rememberMeSwitch != null) {
                rememberMeSwitch.Dispose ();
                rememberMeSwitch = null;
            }

            if (rememberMeText != null) {
                rememberMeText.Dispose ();
                rememberMeText = null;
            }

            if (Title != null) {
                Title.Dispose ();
                Title = null;
            }

            if (UiIndicator != null) {
                UiIndicator.Dispose ();
                UiIndicator = null;
            }

            if (Warning != null) {
                Warning.Dispose ();
                Warning = null;
            }
        }
    }
}
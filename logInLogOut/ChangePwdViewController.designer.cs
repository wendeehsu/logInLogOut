// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace logInLogOut
{
    [Register ("ChangePwdViewController")]
    partial class ChangePwdViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton cancelBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton confirmBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField confirmPwdTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton HideModelBtn { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField newPasswordTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField oldPasswordTextField { get; set; }

        [Action ("CancelBtn_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void CancelBtn_TouchUpInside (UIKit.UIButton sender);

        [Action ("ConfirmBtn_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void ConfirmBtn_TouchUpInside (UIKit.UIButton sender);

        [Action ("HideModelBtn_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void HideModelBtn_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (cancelBtn != null) {
                cancelBtn.Dispose ();
                cancelBtn = null;
            }

            if (confirmBtn != null) {
                confirmBtn.Dispose ();
                confirmBtn = null;
            }

            if (confirmPwdTextField != null) {
                confirmPwdTextField.Dispose ();
                confirmPwdTextField = null;
            }

            if (HideModelBtn != null) {
                HideModelBtn.Dispose ();
                HideModelBtn = null;
            }

            if (newPasswordTextField != null) {
                newPasswordTextField.Dispose ();
                newPasswordTextField = null;
            }

            if (oldPasswordTextField != null) {
                oldPasswordTextField.Dispose ();
                oldPasswordTextField = null;
            }
        }
    }
}
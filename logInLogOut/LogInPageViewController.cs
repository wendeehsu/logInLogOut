using Foundation;
using Newtonsoft.Json;
using Plugin.SecureStorage;
using System;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using UIKit;

namespace logInLogOut
{
    public partial class LogInPageViewController : UIViewController
    {
        public LogInPageViewController (IntPtr handle) : base (handle)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            //Get token
            var plist = NSUserDefaults.StandardUserDefaults;
            var useHeader = plist.StringForKey("userToken");
            GetUserInfo(useHeader);
        }

        public static UIImage UIImageFromUrl(string uri)
        {
            using (var url = new NSUrl(uri))
            using (var data = NSData.FromUrl(url))
                return UIImage.LoadFromData(data);
        }

        public class Img
        {
            public int id { get; set; }
            public string url { get; set; }
        }

        async public void Carousel(Img[] pictures)
        {
            if (pictures.Length > 1)
            {
                while (true)
                {
                    foreach (Img i in pictures)
                    {
                        userImg.Image = UIImageFromUrl(i.url);
                        await Task.Delay(1500);
                    }
                }
            }
            else if (pictures.Length == 1)
            {
                userImg.Image = UIImageFromUrl(pictures[0].url);
            }
            else
            {
                userImg.Hidden = true;
                var text = new UILabel();
                text.Text = "Oops, you don't have pictures.";
                View.AddSubview(text);
            }
        }

        async public void GetUserInfo(string t)
        {
            try
            {
                //use token to request user info
                var client = new HttpClient();
                string url = "http://sample.dentall.io/api/photos/";
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", t);
                var response = await client.GetAsync(url);
                //Debug.WriteLine(response);
                int status = (int)response.StatusCode;
                if (status == 200)
                {
                    var result = await response.Content.ReadAsStringAsync();
                    Img[] imgs = JsonConvert.DeserializeObject<Img[]>(result);

                    Debug.WriteLine(result);
                    Carousel(imgs);
                }
            }
            catch(Exception ex){
                var alert = UIAlertController.Create("Oops!", ex.Message, UIAlertControllerStyle.Alert);

                //another way: do nothing when the option is clicked
                alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

                //display the alert as a popover view on top of the current view
                PresentViewController(alert, true, null);
            }
        }



        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        partial void LogOutBtn_TouchUpInside(UIButton sender)
        {
            var plist = NSUserDefaults.StandardUserDefaults;
            plist.RemoveObject("userToken");
            Debug.WriteLine($"this : {plist.StringForKey("userToken")}");
            this.NavigationController.PopViewController(true);
        }
    }
}